package com.example.tom.timer

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.PictureDrawable
import android.widget.ImageView
import android.widget.TextView
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGParseException
import net.gnehzr.tnoodle.scrambles.Puzzle
import net.gnehzr.tnoodle.svglite.Svg
import puzzle.*

fun newScramble(puzzleType: String): String {
    val puzzle: Puzzle
    when (puzzleType) {
        "2" -> puzzle = TwoByTwoCubePuzzle()
        "3" -> puzzle = ThreeByThreeCubePuzzle()
        "3FM" -> puzzle = ThreeByThreeCubeFewestMovesPuzzle()
        "4" -> puzzle = FourByFourCubePuzzle()
        "5" -> puzzle = CubePuzzle(5)
        "6" -> puzzle = CubePuzzle(6)
        "7" -> puzzle = CubePuzzle(7)
        "Pyra" -> puzzle = PyraminxPuzzle()
        "Mega" -> puzzle = MegaminxPuzzle()
        "SQ1" -> puzzle = SquareOnePuzzle()
        "Clock" -> puzzle = ClockPuzzle()
        "Skewb" -> puzzle = SkewbPuzzle()
        else ->
            puzzle = ThreeByThreeCubePuzzle()
    }
    return puzzle.generateScramble()
}

fun newSvg(scramble: String, puzzleType: String): Svg? {
    val puzzle: Puzzle
    when (puzzleType) {
        "2" -> puzzle = TwoByTwoCubePuzzle()
        "3" -> puzzle = ThreeByThreeCubePuzzle()
        "3FM" -> puzzle = ThreeByThreeCubeFewestMovesPuzzle()
        "4" -> puzzle = FourByFourCubePuzzle()
        "5" -> puzzle = CubePuzzle(5)
        "6" -> puzzle = CubePuzzle(6)
        "7" -> puzzle = CubePuzzle(7)
        "Pyra" -> puzzle = PyraminxPuzzle()
        "Mega" -> puzzle = MegaminxPuzzle()
        "SQ1" -> puzzle = SquareOnePuzzle()
        "Clock" -> puzzle = ClockPuzzle()
        "Skewb" -> puzzle = SkewbPuzzle()
        else -> puzzle = ThreeByThreeCubePuzzle()
    }
    try {
        return puzzle.drawScramble(scramble, null)
    } catch (e: Exception) {
        return null
    }
}

fun svgToDrawable(scrambleSvg: Svg?): Drawable? {
    try {
        val svgScramble = SVG.getFromString(scrambleSvg.toString())
        return PictureDrawable(svgScramble.renderToPicture())
    } catch (e: SVGParseException) {
        return null
    }
}

fun newScrambleTask(appContext: Context, textViewScramble: TextView?, imageViewScramble: ImageView?): ScrambleTask {
    return ScrambleTask(appContext, textViewScramble, imageViewScramble)
}

