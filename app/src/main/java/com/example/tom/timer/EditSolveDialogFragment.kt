package com.example.tom.timer

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView

class EditSolveDialogFragment : DialogFragment(), AdapterView.OnItemSelectedListener {

    private var solveId: Long = 0
    lateinit private var solve: Solve

    lateinit private var textViewTime: TextView

    lateinit internal var mListener: NoticeDialogListener

    fun setSolveData(solve: Solve, solveId: Long) {
        this.solveId = solveId
        this.solve = solve
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {

        val viewDialog = View.inflate(activity, R.layout.spinner_penalty, null)
        val spinnerPenalty = viewDialog.findViewById(R.id.spinnerPenalty) as Spinner
        val adapterSpinnerPenalty = ArrayAdapter.createFromResource(context,
                R.array.arraySpinnerPenalty, R.layout.spinner_penalty_row)
        spinnerPenalty.adapter = adapterSpinnerPenalty
        spinnerPenalty.onItemSelectedListener = this
        spinnerPenalty.setSelection(solve.solveType)

        textViewTime = viewDialog.findViewById(R.id.dialogTime) as TextView
        updateTime()

        val textViewScramble = viewDialog.findViewById(R.id.dialogScramble) as TextView
        textViewScramble.text = solve.scramble

        val builder = AlertDialog.Builder(activity)
        builder.setView(viewDialog)
                .setPositiveButton(R.string.delete) { dialog, id -> mListener.onDialogPositiveClick(this@EditSolveDialogFragment, solveId) }
                .setNegativeButton(R.string.cancel) { dialog, id -> mListener.onDialogNegativeClick(this@EditSolveDialogFragment) }
                .setNeutralButton(R.string.save) { dialog, id -> mListener.onDialogNeutralClick(this@EditSolveDialogFragment, solveId, solve) }

        return builder.create()
    }

    interface NoticeDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment, solveId: Long)
        fun onDialogNegativeClick(dialog: DialogFragment)
        fun onDialogNeutralClick(dialog: DialogFragment, id: Long, solve: Solve)
    }

    override fun onAttach(SessionActivity: Activity) {
        super.onAttach(SessionActivity)
        try {
            mListener = SessionActivity as NoticeDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(SessionActivity.toString() + " must implement NoticeDialogListener")
        }
    }

    override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
        this.solve.setType(i)
        updateTime()
    }

    override fun onNothingSelected(adapterView: AdapterView<*>) {}

    private fun updateTime() {
        textViewTime.text = this.solve.toString()
    }
}
