package com.example.tom.timer

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.AsyncTask
import android.os.Handler
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class Timer(
        var tvTimer: TextView, var timerHandler: Handler,
        var tvScramble: TextView, var spSettings: SharedPreferences,
        var appContext: Context, var toolbarDown: Toolbar,
        var toolbarMain: Toolbar, var ivScramble: ImageView,
        var mainActivity: Activity, var solveDbHandler: SolveDbHandler
) {

    private val inspectionLength: Long = 15000
    private val plusTwo: Long = -2000

    private var timerRunning = false
    private var timerJustRun = false
    private var haveInspection = false
    private var updateOn: Boolean = false
    private var inspectionOn: Boolean = false
    private var timeStart: Long = 0
    private var finalTime: Long = 0
    private var solveType = 0

    var ignoreScrambling = true
    var ignoreTouch = false

    lateinit var currentScrambleTask: ScrambleTask
    lateinit var currentScramble: String

    init {
        newScramble()
        updateScramble()
    }

    fun updateSettings() {
        inspectionOn = spSettings.getBoolean("inspection", false)
        updateOn = spSettings.getBoolean("update", true)
    }

    private fun updateScramble() {
        currentScramble = spSettings.getString("currentScramble", "")
    }

    fun checkIfScrambling(): Boolean {
        return currentScrambleTask.status == AsyncTask.Status.RUNNING
    }

    private fun newScramble() {
        currentScrambleTask = newScrambleTask(appContext, tvScramble, ivScramble)
        currentScrambleTask.execute()
    }

    fun touchDown() {
        if (!timerRunning || haveInspection) {
            prepareSolve()
        } else {
            finalizeSolve()
        }
    }

    fun touchUp() {
        tvTimer.setTextColor(Color.BLACK)

        if (!timerRunning && !timerJustRun && !haveInspection) {
            launchTimer()
            newScramble()
            timerRunning = true
        } else if (haveInspection) {
            launchTimer()
        } else {
            timerJustRun = false
            ignoreScrambling = true
        }
    }

    fun prepareSolve() {
        tvTimer.setTextColor(Color.GREEN)
        tvScramble.visibility = View.GONE
        hideActionBars()

        updateScramble()
    }

    fun launchTimer() {
        mainActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED

        timeStart = System.currentTimeMillis()
        ignoreScrambling = false

        if (inspectionOn && !haveInspection) {
            haveInspection = true
            timerHandler.post(inspection)
            return
        }

        haveInspection = false

        if (updateOn) {
            timerHandler.post(timerUpdate)
        } else {
            timerHandler.post(timerNoUpdate)
        }
    }

    fun finalizeSolve() {

        if (solveType == 1) {
            finalTime += 2000
        }

        val newSolve = Solve(finalTime, currentScramble, solveType)

        saveSolve(newSolve)
        resetState(newSolve)
    }

    fun resetState(solve: Solve) {
        tvTimer.text = solve.toString()
        showActionBars()
        tvScramble.visibility = View.VISIBLE
        mainActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR

        timerRunning = false
        timerJustRun = false
        haveInspection = false
        ignoreScrambling = true
        solveType = 0
    }

    fun saveSolve(solve: Solve) {
        solveDbHandler.addSolve(solve, spSettings.getString("currentEvent", "3x3"))
    }

    private val timerUpdate: Runnable = object : Runnable {
        override fun run() {
            if (timerRunning) {
                finalTime = System.currentTimeMillis() - timeStart
                tvTimer.text = MillisToTime(finalTime)
                timerHandler.postDelayed(this, 10)
            }
        }
    }

    private val timerNoUpdate: Runnable = object : Runnable {
        override fun run() {
            if (timerRunning) {
                tvTimer.text = (appContext.getString(R.string.solve))
                finalTime = System.currentTimeMillis() - timeStart
                timerHandler.postDelayed(this, 10)
            }
        }
    }

    private val inspection: Runnable = object : Runnable {
        override fun run() {
            finalTime = System.currentTimeMillis() - timeStart
            val timeLeft = inspectionLength - finalTime
            if (timeLeft > 0) {
                tvTimer.text = MillisToTime(timeLeft)
            } else if (timeLeft > plusTwo) {
                tvTimer.text = "+2"
                solveType = 1
            } else {
                haveInspection = false
                solveType = 2
                finalTime = -1
                finalizeSolve()
            }
            if (haveInspection) {
                timerHandler.postDelayed(this, 10)
            }
        }
    }

    private fun showActionBars() {
        toolbarDown.visibility = View.VISIBLE
        toolbarMain.visibility = View.VISIBLE
    }

    private fun hideActionBars() {
        toolbarDown.visibility = View.GONE
        toolbarMain.visibility = View.GONE
    }
}
