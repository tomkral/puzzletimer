package com.example.tom.timer

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog

class ClearSessionDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.clearSessionDialog)
                .setTitle(R.string.clearSession)
                .setPositiveButton(R.string.delete) { dialog, id -> mListener?.onDialogPositiveClick(this@ClearSessionDialogFragment) }
                .setNegativeButton(R.string.cancel) { dialog, id -> mListener?.onDialogNegativeClick(this@ClearSessionDialogFragment) }
        return builder.create()
    }

    internal var mListener: NoticeDialogListener? = null

    interface NoticeDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onAttach(SessionActivity: Activity?) {
        super.onAttach(SessionActivity)
        try {
            mListener = SessionActivity as NoticeDialogListener?
        } catch (e: ClassCastException) {
            throw ClassCastException(SessionActivity!!.toString() + " must implement NoticeDialogListener")
        }

    }

}
