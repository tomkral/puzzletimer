package com.example.tom.timer

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.Toolbar
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var activity_main: RelativeLayout

    lateinit var tvTimer: TextView
    lateinit var tvScramble: TextView
    lateinit var ivScramble: ImageView
    lateinit var toolbarMain: Toolbar
    lateinit var toolbarDown: Toolbar
    lateinit var intentSession: Intent
    lateinit var intentSettings: Intent
    lateinit var timerHandler: Handler
    lateinit var pMChoosePuzzle: PopupMenu
    lateinit var sPSettingsEditor: SharedPreferences.Editor
    lateinit var sPSettings: SharedPreferences
    lateinit var mainTimer: Timer
    lateinit var currentScrambleTask: ScrambleTask
    lateinit var solveDbHandler: SolveDbHandler

    @SuppressLint("CommitPrefEdits")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activity_main = findViewById(R.id.activity_main) as RelativeLayout

        sPSettings = PreferenceManager.getDefaultSharedPreferences(this)
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        sPSettingsEditor = sPSettings.edit()
        sPSettingsEditor.apply()

        timerHandler = Handler(Looper.getMainLooper())

        solveDbHandler = SolveDbHandler(applicationContext, null)

        ivScramble = findViewById(R.id.imageViewScramble) as ImageView
        ivScramble.visibility = View.GONE

        tvTimer = findViewById(R.id.textViewTimer) as TextView
        tvTimer.keyListener = null

        tvScramble = findViewById(R.id.textViewScramble) as TextView

        toolbarMain = findViewById(R.id.toolbarMainSession) as Toolbar
        setSupportActionBar(toolbarMain)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbarDown = findViewById(R.id.toolbarDown) as Toolbar

        currentScrambleTask = newScrambleTask(this.applicationContext, tvScramble, ivScramble)

        val buttonToSession = findViewById(R.id.buttonToSession) as Button
        val buttonDraw = findViewById(R.id.buttonDraw) as Button
        val buttonChoosePuzzle = findViewById(R.id.buttonChoosePuzzle) as Button
        val buttonToSettings = findViewById(R.id.buttonToSettings) as Button

        pMChoosePuzzle = PopupMenu(this, buttonToSession)
        val inflater = pMChoosePuzzle.menuInflater
        inflater.inflate(R.menu.menu_choosepuzzle, pMChoosePuzzle.menu)

        mainTimer = Timer(tvTimer, timerHandler,
                tvScramble, sPSettings,
                applicationContext, toolbarDown,
                toolbarMain, ivScramble,
                this, solveDbHandler)

        activity_main.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            if (ivScramble.visibility == View.VISIBLE ||
                    (mainTimer.checkIfScrambling() && mainTimer.ignoreScrambling)) {
                return@OnTouchListener true
            }

            mainTimer.updateSettings()

            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                mainTimer.touchDown()
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
                mainTimer.touchUp()
            }

            true
        })

        buttonToSession.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                intentSession = Intent(applicationContext, SessionActivity::class.java)
                startActivity(intentSession)
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
            }
            false
        }

        buttonDraw.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                drawScramble()
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
            }
            false
        }

        buttonChoosePuzzle.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                pMChoosePuzzle.show()
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
            }
            false
        }

        buttonToSettings.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                intentSettings = Intent(applicationContext, SettingsActivity::class.java)
                startActivity(intentSettings)
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
            }
            false
        }

        ivScramble.setOnClickListener { view ->
            if (ivScramble.visibility == View.VISIBLE) {
                ivScramble.visibility = View.GONE
            }
        }

        pMChoosePuzzle.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menuTwo -> {
                    sPSettingsEditor.putString("currentEvent", "2x2")
                    sPSettingsEditor.putString("currentScrambler", "2")
                }
                R.id.menuThree -> {
                    sPSettingsEditor.putString("currentEvent", "3x3")
                    sPSettingsEditor.putString("currentScrambler", "3")
                }
                R.id.menuThreeOH -> {
                    sPSettingsEditor.putString("currentEvent", "3x3oh")
                    sPSettingsEditor.putString("currentScrambler", "3")
                }
                R.id.menuThreeMulti -> {
                    sPSettingsEditor.putString("currentEvent", "3x3mbld")
                    sPSettingsEditor.putString("currentScrambler", "3")
                }
                R.id.menuThreeFeet -> {
                    sPSettingsEditor.putString("currentEvent", "3x3feet")
                    sPSettingsEditor.putString("currentScrambler", "3")
                }
                R.id.menuThreeFM -> {
                    sPSettingsEditor.putString("currentEvent", "3x3fm")
                    sPSettingsEditor.putString("currentScrambler", "3FM")
                }
                R.id.menuThreeBLD -> {
                    sPSettingsEditor.putString("currentEvent", "3x3bld")
                    sPSettingsEditor.putString("currentScrambler", "3")
                }
                R.id.menuFour -> {
                    sPSettingsEditor.putString("currentEvent", "4x4")
                    sPSettingsEditor.putString("currentScrambler", "4")
                }
                R.id.menuFourBLD -> {
                    sPSettingsEditor.putString("currentEvent", "4x4bld")
                    sPSettingsEditor.putString("currentScrambler", "4")
                }
                R.id.menuFive -> {
                    sPSettingsEditor.putString("currentEvent", "5x5")
                    sPSettingsEditor.putString("currentScrambler", "5")
                }
                R.id.menuFiveBLD -> {
                    sPSettingsEditor.putString("currentEvent", "5x5bld")
                    sPSettingsEditor.putString("currentScrambler", "5")
                }
                R.id.menuSix -> {
                    sPSettingsEditor.putString("currentEvent", "6x6")
                    sPSettingsEditor.putString("currentScrambler", "6")
                }
                R.id.menuSeven -> {
                    sPSettingsEditor.putString("currentEvent", "7x7")
                    sPSettingsEditor.putString("currentScrambler", "7")
                }
                R.id.menuClock -> {
                    sPSettingsEditor.putString("currentEvent", "clock")
                    sPSettingsEditor.putString("currentScrambler", "Clock")
                }
                R.id.menuSkweb -> {
                    sPSettingsEditor.putString("currentEvent", "skewb")
                    sPSettingsEditor.putString("currentScrambler", "Skewb")
                }
                R.id.menuSQ1 -> {
                    sPSettingsEditor.putString("currentEvent", "square_one")
                    sPSettingsEditor.putString("currentScrambler", "SQ1")
                }
                R.id.menuPyra -> {
                    sPSettingsEditor.putString("currentEvent", "pyraminx")
                    sPSettingsEditor.putString("currentScrambler", "Pyra")
                }
                R.id.menuMega -> {
                    sPSettingsEditor.putString("currentEvent", "megaminx")
                    sPSettingsEditor.putString("currentScrambler", "Mega")
                }
            }

            sPSettingsEditor.apply()
            updateScramble()
            true
        }
    }

    private fun switchListener(){

    }

    private fun drawScramble() {
        if (ivScramble.visibility == View.GONE) {
            ivScramble.visibility = View.VISIBLE
        } else {
            ivScramble.visibility = View.GONE
        }
    }

    private fun updateScramble() {
        if (currentScrambleTask.status == AsyncTask.Status.RUNNING) {
            currentScrambleTask.cancel(true)
        }

        currentScrambleTask = newScrambleTask(this.applicationContext, tvScramble, ivScramble)
        currentScrambleTask.execute()
    }
}
