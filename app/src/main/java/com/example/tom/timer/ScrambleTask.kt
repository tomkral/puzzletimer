package com.example.tom.timer

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.preference.PreferenceManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class ScrambleTask(private val appContext: Context, private val textViewScramble: TextView?, private val imageViewScramble: ImageView?) : AsyncTask<String, Int, Unit>() {
    private var scramble: String = ""
    private var drawableScramble: Drawable? = null

    override fun onPreExecute() {
        textViewScramble?.setText(R.string.scrambling)
        super.onPreExecute()
    }

    override fun doInBackground(vararg p0: String?): Unit {
        val sPSettings = PreferenceManager.getDefaultSharedPreferences(appContext)
        val sPSettingsEditor = sPSettings.edit()

        val puzzleType = sPSettings.getString("currentScrambler", "3")

        scramble = newScramble(puzzleType)
        drawableScramble = svgToDrawable(newSvg(scramble, puzzleType))

        sPSettingsEditor.putString("currentScramble", scramble).apply()
        return Unit
    }


    override fun onPostExecute(result: Unit?) {
        textViewScramble?.text = scramble
        imageViewScramble?.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        imageViewScramble?.setImageDrawable(drawableScramble)

        super.onPostExecute(result)
    }

    override fun onCancelled() {
        super.onCancelled()
    }
}
