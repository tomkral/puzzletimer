package com.example.tom.timer

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView

class SessionActivity : AppCompatActivity(), ClearSessionDialogFragment.NoticeDialogListener, EditSolveDialogFragment.NoticeDialogListener {

    var selectedSolveID: Long = 0
    lateinit var adapterSolves: SolvesAdapter
    lateinit var adapterStats: ArrayAdapter<*>
    lateinit var sessionAvg: SessionAvg
    lateinit var dfCTS: ClearSessionDialogFragment
    lateinit var dfES: EditSolveDialogFragment
    lateinit var fragmentManager: FragmentManager
    lateinit var solveDbHandler: SolveDbHandler
    lateinit var sPSettings: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)

        val toolbar = findViewById(R.id.toolbarMainSession) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val TimeListToolBar = findViewById(R.id.toolbarListViewTimes) as Toolbar
        setSupportActionBar(TimeListToolBar)
        TimeListToolBar.title = ""
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.hide()

        sPSettings = PreferenceManager.getDefaultSharedPreferences(this)

        solveDbHandler = SolveDbHandler(applicationContext, null)

        sessionAvg = SessionAvg(applicationContext, solveDbHandler.getTimes(sPSettings.getString("currentEvent", "3x3")))

        adapterSolves = SolvesAdapter(this, solveDbHandler.getSolves(sPSettings.getString("currentEvent", "3x3")))
        val lvTimes = findViewById(R.id.listViewTimes) as ListView
        lvTimes.adapter = adapterSolves

        adapterStats = ArrayAdapter(this, R.layout.statslistrow, sessionAvg.alStats)
        val lvStats = findViewById(R.id.listViewStats) as ListView
        lvStats.adapter = adapterStats

        lvTimes.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            val currentCursor = solveDbHandler.getSolves(sPSettings.getString("currentEvent", "3x3"))
            currentCursor.moveToPosition(i)

            val timeMillis = currentCursor.getLong(currentCursor.getColumnIndexOrThrow("_time"))
            val scramble = currentCursor.getString(currentCursor.getColumnIndexOrThrow("_scramble"))
            val solveType = currentCursor.getInt(currentCursor.getColumnIndexOrThrow("_solvetype"))
            val origSolveType = currentCursor.getInt(currentCursor.getColumnIndexOrThrow("_origsolvetype"))
            selectedSolveID = currentCursor.getLong(currentCursor.getColumnIndexOrThrow("_id"))

            currentCursor.close()

            val solve = Solve(timeMillis, scramble, solveType, origSolveType)

            dfES = EditSolveDialogFragment()
            dfES.setSolveData(solve, selectedSolveID)
            dfES.show(fragmentManager, "dialog")
        }

        fragmentManager = supportFragmentManager

        val buttonClearSession = findViewById(R.id.buttonClearSession) as Button
        buttonClearSession.setOnClickListener { view ->
            dfCTS = ClearSessionDialogFragment()
            dfCTS.show(fragmentManager, "dialog")
        }

    }

    override fun onResume() {
        super.onResume()
        sessionAvg.updateStats()
    }

    fun updateAdapters() {
        sessionAvg.updateTimes(solveDbHandler.getTimes(sPSettings.getString("currentEvent", "3x3")))
        sessionAvg.updateStats()

        val newCursor = solveDbHandler.getSolves(sPSettings.getString("currentEvent", "3x3"))

        adapterSolves.notifyCursorChanged(newCursor)
        adapterStats.notifyDataSetChanged()
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        solveDbHandler.deleteTable(sPSettings.getString("currentEvent", "3x3"))
        updateAdapters()
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, solveId: Long) {
        solveDbHandler.deleteSolve(sPSettings.getString("currentEvent", "3x3"), solveId)
        updateAdapters()
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {}

    override fun onDialogNeutralClick(dialog: DialogFragment, id: Long, solve: Solve) {
        val currentEvent = sPSettings.getString("currentEvent", "3x3")

        solveDbHandler.modifySolve(solve, id, currentEvent)

        updateAdapters()
    }
}
