package com.example.tom.timer

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

class SolveDbHandler(context: Context,
                     factory: SQLiteDatabase.CursorFactory?) : SQLiteOpenHelper(context,
        SolveDbHandler.DATABASE_NAME, factory,
        SolveDbHandler.DATABASE_VERSION) {

    private val ArrayListEventTables = ArrayList(Arrays.asList(
            TABLE_2x2, TABLE_3x3, TABLE_3x3_BLD, TABLE_3x3_FEET, TABLE_3x3_FM, TABLE_3x3_MBLD,
            TABLE_3x3_OH, TABLE_4x4, TABLE_4x4_BLD, TABLE_5x5, TABLE_5x5_BLD, TABLE_6x6,
            TABLE_7x7, TABLE_MEGAMINX, TABLE_PYRAMINX, TABLE_CLOCK, TABLE_SKEWB, TABLE_SQ1))

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        for (event_table in ArrayListEventTables) {
            sqLiteDatabase.execSQL(newQuery(event_table))
        }
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        for (event_table in ArrayListEventTables) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXIST" + event_table)
        }

        onCreate(sqLiteDatabase)
    }

    fun addSolve(solve: Solve, event: String) {
        val solveValues = ContentValues()
        solveValues.put(COLUMN_TIME, solve.timeMillis)
        solveValues.put(COLUMN_SCRAMBLE, solve.scramble)
        solveValues.put(COLUMN_SOLVE_TYPE, solve.solveType)
        solveValues.put(COLUMN_ORIG_SOLVE_TYPE, solve.origSolveType)

        val sqLiteDatabase = writableDatabase
        sqLiteDatabase.insert(eventToTable(event), null, solveValues)
        sqLiteDatabase.close()
    }

    fun modifySolve(solve: Solve, id: Long, event: String) {
        val sqLiteDatabase = writableDatabase
        val solveValues = ContentValues()

        solveValues.put(COLUMN_TIME, solve.timeMillis)
        solveValues.put(COLUMN_SOLVE_TYPE, solve.solveType)

        sqLiteDatabase.update(eventToTable(event), solveValues, "_id=" + id, null)
        sqLiteDatabase.close()
    }

    fun getSolves(event: String): Cursor {
        val sqLiteDatabase = readableDatabase

        val projection = arrayOf(COLUMN_ID, COLUMN_TIME, COLUMN_SCRAMBLE, COLUMN_SOLVE_TYPE, COLUMN_ORIG_SOLVE_TYPE)

        return sqLiteDatabase.query(
                eventToTable(event),
                projection,
                null,
                null,
                null,
                null,
                "_id DESC"
        )
    }

    fun getTimes(event: String): Cursor {
        val sqLiteDatabase = readableDatabase

        val projection = arrayOf(COLUMN_TIME)

        return sqLiteDatabase.query(
                eventToTable(event),
                projection,
                null,
                null,
                null,
                null,
                null
        )
    }

    fun deleteTable(event: String) {
        val sqLiteDatabase = readableDatabase
        sqLiteDatabase.execSQL("DELETE FROM " + eventToTable(event))
    }

    fun deleteSolve(event: String, id: Long) {
        val sqLiteDatabase = readableDatabase
        sqLiteDatabase.delete(eventToTable(event), "_id" + " =  '" + id.toString() + "' ", null)
    }

    //TODO watch out for query changes
    private fun newQuery(event: String): String {
        return """CREATE TABLE IF NOT EXISTS $event ("
        +COLUMN_ID + " INTEGER PRIMARY KEY, "
        +COLUMN_TIME + " INTEGER, "
        +COLUMN_SCRAMBLE + " TEXT, "
        +COLUMN_SOLVE_TYPE + " INTEGER, "
        +COLUMN_ORIG_SOLVE_TYPE + " INTEGER" + "" +");"""
    }

    private fun eventToTable(event: String): String {
        return "T" + event
    }

    companion object {

        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "solves.db"

        val TABLE_2x2 = "T2x2"
        val TABLE_3x3 = "T3x3"
        val TABLE_3x3_BLD = "T3x3bld"
        val TABLE_3x3_OH = "T3X3oh"
        val TABLE_3x3_FEET = "T3x3feet"
        val TABLE_3x3_FM = "T3x3fm"
        val TABLE_3x3_MBLD = "T3x3mbld"
        val TABLE_4x4 = "T4x4"
        val TABLE_4x4_BLD = "T4x4bld"
        val TABLE_5x5 = "T5x5"
        val TABLE_5x5_BLD = "T5x5bld"
        val TABLE_6x6 = "T6x6"
        val TABLE_7x7 = "T7x7"
        val TABLE_MEGAMINX = "Tmegaminx"
        val TABLE_PYRAMINX = "Tpyraminx"
        val TABLE_CLOCK = "Tclock"
        val TABLE_SKEWB = "Tskewb"
        val TABLE_SQ1 = "Tsquare_one"

        private val COLUMN_ID = "_id"
        private val COLUMN_TIME = "_time"
        private val COLUMN_SCRAMBLE = "_scramble"
        private val COLUMN_SOLVE_TYPE = "_solvetype"
        private val COLUMN_ORIG_SOLVE_TYPE = "_origsolvetype"
    }
}
