package com.example.tom.timer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val toolbar = findViewById(R.id.toolbarPreferences) as Toolbar
        toolbar.title = "Settings"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val settingsFM = fragmentManager
        val settingsFT = settingsFM.beginTransaction()

        settingsFT.replace(R.id.fragment_container, SettingsFragment())
        settingsFT.commit()
    }
}
