package com.example.tom.timer

class Solve(var timeMillis: Long, var scramble: String, var solveType: Int, val origSolveType: Int = solveType) {

    override fun toString(): String {
        when (solveType) {
            0 -> return MillisToTime(timeMillis)
            1 -> return MillisToTime(timeMillis) + " (+2)"
            2 -> {
                if (timeMillis < 0) {
                    return "DNF"
                } else {
                    return MillisToTime(timeMillis) + " (DNF)"
                }
            }
            else -> return MillisToTime(timeMillis)
        }
    }

    fun setType(newType: Int) {
        if (newType != this.solveType) {
            when (newType) {
                0 -> {
                    if (this.solveType == 1) {
                        this.timeMillis -= 2000
                    }
                }
                1 -> {
                    timeMillis += 2000
                }
                2 -> {
                    if (this.solveType == 1) {
                        this.timeMillis -= 2000
                    }
                }
            }
            this.solveType = newType
        }
    }
}
