package com.example.tom.timer

import android.content.Context
import android.database.Cursor
import java.util.*


class SessionAvg(internal var appContext: Context, cursorTimes: Cursor) {

    var alStats: ArrayList<String> = ArrayList<String>()
    private val alTimes: ArrayList<Long> = ArrayList<Long>()
    private val AL_AVGS_SIZES: ArrayList<Int> = ArrayList(Arrays.asList(5, 12, 50, 100, 1000))

    init {
        updateTimes(cursorTimes)
    }

    internal fun updateTimes(cursorTimes: Cursor) {
        alTimes.clear()

        while (cursorTimes.moveToNext()) {
            val itemId = cursorTimes.getLong(
                    cursorTimes.getColumnIndexOrThrow("_time"))

            alTimes.add(0, itemId)
        }

        cursorTimes.close()
    }

    private fun solveCount(): String {
        return appContext.getString(R.string.solveCount) + alTimes.size.toString()
    }

    private fun bestTime(): String {
        if (alTimes.isEmpty()) {
            return appContext.getString(R.string.best) + "0"
        } else {
            return appContext.getString(R.string.best) + MillisToTime(alTimes.min())
        }
    }

    private fun worstTime(): String {
        if (alTimes.isEmpty()) {
            return appContext.getString(R.string.worst) + "0"
        }
        return appContext.getString(R.string.worst) + MillisToTime(alTimes.max())
    }

    private fun totalMean(): String {
        val validTimes: List<Long> = alTimes.filter { num -> num > 0 }

        if (validTimes.isEmpty()) {
            return appContext.getString(R.string.totalMean) + "0"
        }

        val totalMean: Long = validTimes.sum() / validTimes.size

        return appContext.getString(R.string.totalMean) + MillisToTime(totalMean)
    }

    private fun lastAvg(length: Int): String? {
        if (alTimes.size < length) {
            return null
        }

        val alSub = ArrayList(alTimes.subList(0, length))

        if (avg(alSub) != null) {
            return appContext.getString(R.string.lastAvg) + length.toString() + ": " + MillisToTime(avg(alSub))
        } else {
            return appContext.getString(R.string.lastAvg) + length.toString() + ": " + "DNF"
        }
    }

    private fun bestAvg(length: Int): String? {
        val alAvgs = ArrayList<Long>()
        val maxSize = alTimes.size

        if (maxSize < length + 1) {
            return null
        }

        var pos = 0

        while (pos + length <= maxSize) {
            val subAvgList = ArrayList(alTimes.subList(pos, pos + length))

            if (avg(subAvgList) != null){
                alAvgs.add(avg(subAvgList) as Long)
            }

            pos++
        }
        if (alAvgs.isEmpty()){
            return appContext.getString(R.string.bestAvg) + length.toString() + ": " + "DNF"
        } else {
            return appContext.getString(R.string.bestAvg) + length.toString() + ": " + MillisToTime(alAvgs.min())
        }
    }

    private fun avg(alAvgTimes: ArrayList<Long>): Long? {
        val length = alAvgTimes.size

        val validTimes: List<Long> = alAvgTimes.filter { num -> num > 0 }
        var totalSum: Long = validTimes.sum()


        if (validTimes.size < length - 1) {
            return null
        } else {
            if (alAvgTimes.size == length) {
                totalSum -= validTimes.max() as Long
                alAvgTimes.remove(validTimes.max())
            }
            totalSum -= validTimes.min() as Long
            alAvgTimes.remove(validTimes.min())

            return totalSum / length
        }
    }

    fun updateStats() {
        alStats.clear()

        alStats.add(solveCount())
        alStats.add(totalMean())
        alStats.add(worstTime())
        alStats.add(bestTime())

        for (i in AL_AVGS_SIZES) {
            val avg = lastAvg(i)
            val avgBest = bestAvg(i)

            if (avg != null) {
                alStats.add(avg)
            }

            if (avgBest != null) {
                alStats.add(avgBest)
            }
        }
    }
}