package com.example.tom.timer

private fun millisToSecs(millis: Long): String {
    return String.format("%2.3f", millis / 1000.0)
}

private fun millisToMins(Millis: Long): String {
    val raw_minutes: Double = Millis.toDouble() / 1000.0 / 60.0
    val secs: String = String.format("%2.3f", raw_minutes % 1 * 60)

    return raw_minutes.toInt().toString() + ":" + secs.substring(0, secs.length)
}

private fun millisToHours(millis: Long): String {
    val raw_hours: Double = millis.toDouble() / 1000.0 / 60.0 / 60.0
    val raw_minutes: Double = raw_hours % 1 * 60
    val secs: String = String.format("%2.3f", raw_minutes % 1 * 60)

    return (raw_hours.toInt().toString() + ":" + raw_minutes.toInt() + ":" + secs)
}

fun MillisToTime(millis: Long?): String {
    val millis = millis as Long
    if (millis < 60000) {
        return millisToSecs(millis)
    } else {
        if (millis < 3600000) {
            return millisToMins(millis)
        } else {
            return millisToHours(millis)
        }
    }
}
