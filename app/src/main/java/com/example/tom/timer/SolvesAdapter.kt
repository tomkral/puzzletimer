package com.example.tom.timer

import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.TextView

class SolvesAdapter(context: Context, cursor: Cursor) : CursorAdapter(context, cursor, 0) {

    override fun newView(context: Context, cursor: Cursor, viewGroup: ViewGroup): View {
        return LayoutInflater.from(context).inflate(R.layout.solveslistrow, viewGroup, false)
    }

    override fun bindView(view: View, context: Context, cursor: Cursor) {
        val tvTime = view.findViewById(R.id.time) as TextView
        val tvScramble = view.findViewById(R.id.scramble) as TextView

        val time = cursor.getLong(cursor.getColumnIndexOrThrow("_time"))
        val solveType = cursor.getInt(cursor.getColumnIndexOrThrow("_solvetype"))
        val scramble = cursor.getString(cursor.getColumnIndexOrThrow("_scramble"))

        val solve = Solve(time, scramble, solveType)

        tvTime.text = solve.toString()
        tvScramble.text = solve.scramble
    }

    override fun notifyDataSetChanged() {
        super.notifyDataSetChanged()
    }

    fun notifyCursorChanged(newCursor: Cursor) {
        this.notifyDataSetChanged()
        this.changeCursor(newCursor)
    }
}
